const gulp = require("gulp");
const sass = require("gulp-sass");
const sourcemaps = require("gulp-sourcemaps");
const autoprefixer = require("gulp-autoprefixer");
const cleancss = require("gulp-clean-css");

gulp.task("sass-compile", () => {
	return gulp
		.src("./scss/**/*.scss")
		.pipe(sourcemaps.init())
		.pipe(sass().on("error", sass.logError))
		.pipe(
			autoprefixer({
				overrideBrowserslist: ["last 10 versions"],
				grid: true,
			})
		)
		.pipe(cleancss({ format: "beautify" }))
		.pipe(sourcemaps.write("./"))
		.pipe(gulp.dest("./css/"));
});

gulp.task("watch", () => {
	gulp.watch("./scss/**/*.scss", gulp.series("sass-compile"));
});
